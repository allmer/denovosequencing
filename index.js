import Controller from './src/Controller.js';
import fs from "fs";

const controller = new Controller();
const pipeline = (...fns) => (input) => {
    return fns.reduce((acc, f) => {
        return f(acc);
    }, input);
};

const parameters = {'createIonTolerance' : 0.3,'createModelTolerance' :0.3, 'createRawBIonTolerance' :0.3,'finishBIonTolerance' :0.3,'findPathTolerance' :0.4,
    'edgeNames' : ['lossH2OaToc','lossNH3aToc', 'lossH2OaTob','aTocORlossNH3aTob', 'lossH2OyTox', 'lossNH3yTox', 'zTox',
        'lossH2ObToc', 'lossNH3bToc', 'lossH2OaTolossNH3b', 'aTob', 'lossNH3aTolossH2Ob', 'yTox', 'waterLoss', 'amoniumLoss',
        'yToz','aToblossNH3','aToblossH2O','lossH2OyToz', 'lossNH3yToz'],
    'filePath': './data/3spectra.mgf',
    'k' : 3,
    normalization: [{method:'normalizeIntensity',params:[]}],
    spectrum_filtering :[{method:'filterSpectrum',params:[2]}],
};



const settingsID = Date.now();
parameters.settingsID = settingsID;
// fs.writeFileSync('./data/'+ settingsID +'.json', JSON.stringify(parameters));
const text = fs.readFileSync(parameters.filePath, 'utf8');
const spectra = text.split('END IONS');
spectra.pop();
const result = [];
let counter = 0;
spectra.forEach(spectrum =>{
    parameters.spectrum = spectrum.trim().match(/[^\r\n]+/g);
    result.push(pipeline(
        Controller.readInput,
        Controller.normalization,
        Controller.spectrum_filtering,
        Controller.createIonGraph,
        Controller.filterIonGraphNodes,
        Controller.filterIonGraphEdges,
        Controller.createModelGraph,
        Controller.filterModels,
        Controller.createRawBIonGraph,
        Controller.mergeBIons,
        Controller.filterBIons,
        Controller.finishBIonGraph,
        Controller.filterEdges,
        Controller.findPaths,
        Controller.scorePaths,
        Controller.rankPaths,
        Controller.correctScorer,
        Controller.editDistScorer,
        Controller.writeOutput,
    )(parameters));
    counter += 1
    console.log(counter)
});

let avgEditDist = 0;
let correctSize = 0;
result.forEach( r =>{
    avgEditDist += r.editDist / result.length
    correctSize += r.correct / result.length
});

console.log('avgEditDist: ' + avgEditDist)
console.log('correctSize: ' + correctSize)
fs.writeFileSync('./data/result.json', JSON.stringify(result));



