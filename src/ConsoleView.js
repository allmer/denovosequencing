import View from "./View.js";

export default class ConsoleView extends View {
    constructor() {
        super();
    }
    bindUploadFile(handler){}
    bindRed(handler){}
    bindBlue(handler){}
    bindGreen(handler){}
    bindYellow(handler){}
    bindShow(handler){}
    bindBIonGraph(handler){}
}