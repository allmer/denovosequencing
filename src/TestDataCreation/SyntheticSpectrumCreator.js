import * as constants from '../helper/Chemistry.js';

/**
 * This class allows the creation of a synthetic spectrum with selected ion types
 */
class SyntheticSpectrum {
    constructor(sequence, precursorMass, charge) {
        this.sequence = sequence;
        if(this.sequence.length <= 1)
            throw 'sequence must be longer than 1 (provided: ' + sequence + ').';
        this.precursorMass = precursorMass;
        this.charge = charge;
        this.spectrum = [];
    }
    
    generateBIons(genBIons, genWaterLoss, genNH3Loss, genIsotopes, numIsotopes=2, intensity = 100, useMonoIsotopicMass = true, minCharge = 1, maxCharge = this.charge) {
        let scIonMZ = constants.Hydrogen;
        for(let s=0; s<this.sequence.length; s++) {
            if(useMonoIsotopicMass)
                scIonMZ += constants.aminoAcidsByName[this.sequence[s]].monoIsotopicMass;
            else
                scIonMZ += constants.aminoAcidsByName[this.sequence[s]].avgMass;
            for(let z=minCharge; z<=maxCharge; z++) {
                let mz = scIonMZ + (z-1)*constants.Hydrogen;
                mz /= z;
                let neut = constants.Neutron/z;
                if(genBIons) {
                    this.spectrum.push({'mz':mz, 'intensity':intensity, 'ionType':'b', 'pos':(s+1), 'z':z, 'isotope':0});
                    if(genIsotopes) {
                        for(let i=0; i<numIsotopes; i++) {
                            this.spectrum.push({'mz':mz+(i+1)*neut, 'intensity':intensity/(i+2), 'ionType':'b', 'pos':(s+1), 'z':z, 'isotope':(i+1)});
                        }
                    }
                }
                if(genWaterLoss) {
                    let h2o = constants.Water/z;
                    this.spectrum.push({'mz':mz-h2o, 'intensity':intensity, 'ionType':'b-H2O', 'pos':(s+1), 'z':z, 'isotope':0});
                    if(genIsotopes) {
                        for(let i=0; i<numIsotopes; i++) {
                            this.spectrum.push({'mz':mz+(i+1)*neut-h2o, 'intensity':intensity/(i+2), 'ionType':'b-H2O', 'pos':(s+1), 'z':z, 'isotope':(i+1)});
                        }
                    }                    
                }
                if(genNH3Loss) {
                    let nh3 = constants.Amino/z;
                    this.spectrum.push({'mz':mz-nh3, 'intensity':intensity, 'ionType':'b-NH3', 'pos':(s+1), 'z':z, 'isotope':0});
                    if(genIsotopes) {
                        for(let i=0; i<numIsotopes; i++) {
                            this.spectrum.push({'mz':mz+(i+1)*neut-nh3, 'intensity':intensity/(i+2), 'ionType':'b-NH3', 'pos':(s+1), 'z':z, 'isotope':(i+1)});
                        }
                    }                      
                }
            }
        }
    }
}

module.exports = SyntheticSpectrum;

