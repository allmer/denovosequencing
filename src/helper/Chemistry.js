/* 
 * This file contains information about chemical symbols used in this project.
 * It defines masses of the main players.
 */

export const Neutron = {
        mass: 1.00866491588
};

////////////////////////////////////////////////////////////
//
//      Chemical Elements 
//
//////////////////////////////////////////////////////////

export const Hydrogen = {
        code: 'H',      //abbreviation of the element
        monoIsotopicMass : 1.08,    //mass of the typical isotope of the element.
        avgMass : 1.080117005,      //average mass of the element
        abuStabIsotopes : [0.999885,0.000114,0.000001] //only stable isotopes with occurence in nature. 0:typical isotope, 1: +1 Neutron, 2: +2 ...
};

export const Carbon = {
        code: 'C',
        monoIsotopicMass : 12, 
        avgMass : 12.01089358,
        abuStabIsotopes : [0.9893,0.0106,0.0001]
};

export const Oxygen = {
        code: 'O',
        monoIsotopicMass : 15.99491461960, 
        avgMass : 15.99943344,
        abuStabIsotopes : [0.99757,0.00038,0.00205] 
};

export const Nitrogen = {
        code: 'N',
        monoIsotopicMass : 14.00307400446, 
        avgMass : 14.00674554,
        abuStabIsotopes : [0.99636,0.00364]     
};

export const Sulfur = {
        code: 'S',
        monoIsotopicMass : 31.9720711744, 
        avgMass : 32.06577615,
        abuStabIsotopes : [0.9499,0.0075,0.0425,0,0.0001]
};

//////////////////////////////////////////////////////////
//
//      Small Compounds
//
//////////////////////////////////////////////////////////

export const Hydroxyl = {
    code: 'OH',
    monoIsotopicMass : Oxygen.monoIsotopicMass + Hydrogen.monoIsotopicMass,
    avgMass : Oxygen.avgMass + Hydrogen.avgMass
};

export const Water = {
    code: 'H2O',
    monoIsotopicMass : Oxygen.monoIsotopicMass + 2*Hydrogen.monoIsotopicMass,
    avgMass : Oxygen.avgMass + 2*Hydrogen.avgMass    
};

export const Amino = {
    code: 'NH3',
    monoIsotopicMass : Nitrogen.monoIsotopicMass + 3*Hydrogen.monoIsotopicMass,
    avgMass : Nitrogen.avgMass + 3*Hydrogen.avgMass        
};

export const Amine = { //here only R-NH2 where R is typically an amino acid
    code: 'NH2',
    monoIsotopicMass : Nitrogen.monoIsotopicMass + 2*Hydrogen.monoIsotopicMass,
    avgMass : Nitrogen.avgMass + 2*Hydrogen.avgMass        
};

export const Carboxyl = {
    code: 'COOH',
    monoIsotopicMass : Carbon.monoIsotopicMass + 2*Oxygen.monoIsotopicMass + Hydrogen.monoIsotopicMass,
    avgMass : Carbon.avgMass + 2*Oxygen.avgMass + Hydrogen.avgMass    
};

//////////////////////////////////////////////////////////
//
//      Amino acids
//
//////////////////////////////////////////////////////////

export let aminoAcidsByName = []; //contains amino acids in an associative array accessible by single letter code.
export let aminoAcidsByMMass = [];//contains amino acids in an associative array accessible by approximate monoisotopic mass.

aminoAcidsByName['G'] = aminoAcidsByMMass[57] = {
    name : 'Glycine',   //full amino acid name
    tlc : 'Gly',        //three letter code
    slc : 'G',          //single letter code
    monoIsotopicMass : 57.02,   //monoisotopic mass of amino acid
    avgMass: 57.05              //average mass of amino acid
};

aminoAcidsByName['A'] = aminoAcidsByMMass[71] = {
    name : 'Alanine',
    tlc : 'Ala',
    slc : 'A',
    monoIsotopicMass : 71.04,
    avgMass: 71.08
};

aminoAcidsByName['S'] = aminoAcidsByMMass[87] = {
    name : 'Serine',
    tlc : 'Ser',
    slc : 'S',
    monoIsotopicMass : 87.03,
    avgMass: 87.08
};

aminoAcidsByName['P'] = aminoAcidsByMMass[97] = {
    name : 'Proline',
    tlc : 'Pro',
    slc : 'P',
    monoIsotopicMass : 97.05,
    avgMass: 97.12
};

aminoAcidsByName['V'] = aminoAcidsByMMass[99] = {
    name : 'Valine',
    tlc : 'Val',
    slc : 'V',
    monoIsotopicMass : 99.07,
    avgMass: 99.13
};

aminoAcidsByName['T'] = aminoAcidsByMMass[101] = {
    name : 'Threonine',
    tlc : 'Thr',
    slc : 'T',
    monoIsotopicMass : 101.05,
    avgMass: 101.11
};

aminoAcidsByName['C'] = aminoAcidsByMMass[103] = {
    name : 'Cysteine',
    tlc : 'Cys',
    slc : 'C',
    monoIsotopicMass : 103.01,
    avgMass: 103.14
};

aminoAcidsByName['I'] = aminoAcidsByMMass[113] = { //same mass as Leucine so Leucine will override and be used by default.
    name : 'Isoleucine',
    tlc : 'Ile',
    slc : 'I',
    monoIsotopicMass : 113.08,
    avgMass: 113.16
};

aminoAcidsByName['L'] = aminoAcidsByMMass[113] = {
    name : 'Leucine',
    tlc : 'Leu',
    slc : 'L',
    monoIsotopicMass : 113.08,
    avgMass: 113.16
};

aminoAcidsByName['N'] = aminoAcidsByMMass[114] = {
    name : 'Asparagine',
    tlc : 'Asn',
    slc : 'N',
    monoIsotopicMass : 114.04,
    avgMass: 114.10
};

aminoAcidsByName['D'] = aminoAcidsByMMass[115] = {
    name : 'Aspartic Acid',
    tlc : 'Asp',
    slc : 'D',
    monoIsotopicMass : 115.03,
    avgMass: 115.09
};

aminoAcidsByName['Q'] = aminoAcidsByMMass[128] = { //will be superseeded by K as the mass is too similar.
    name : 'Glutamine',
    tlc : 'Gln',
    slc : 'Q',
    monoIsotopicMass : 128.06,
    avgMass: 128.13
};

aminoAcidsByName['K'] = aminoAcidsByMMass[128] = {
    name : 'Lysine',
    tlc : 'Lys',
    slc : 'K',
    monoIsotopicMass : 128.09,
    avgMass: 128.09
};

aminoAcidsByName['E'] = aminoAcidsByMMass[129] = {
    name : 'Glutamic Acid',
    tlc : 'Glu',
    slc : 'E',
    monoIsotopicMass : 129.04,
    avgMass: 129.12
};

aminoAcidsByName['M'] = aminoAcidsByMMass[131] = {
    name : 'Methionine',
    tlc : 'Met',
    slc : 'M',
    monoIsotopicMass : 131.04,
    avgMass: 131.19
};

aminoAcidsByName['H'] = aminoAcidsByMMass[137] = {
    name : 'Histidine',
    tlc : 'His',
    slc : 'H',
    monoIsotopicMass : 137.06,
    avgMass: 137.14
};

aminoAcidsByName['F'] = aminoAcidsByMMass[147] = {
    name : 'Phenylalanine',
    tlc : 'Phe',
    slc : 'F',
    monoIsotopicMass : 147.07,
    avgMass: 147.18
};

aminoAcidsByName['R'] = aminoAcidsByMMass[156] = {
    name : 'Arginine',
    tlc : 'Arg',
    slc : 'R',
    monoIsotopicMass : 156.10,
    avgMass: 156.19
};

aminoAcidsByName['Y'] = aminoAcidsByMMass[163] = {
    name : 'Tyrosine',
    tlc : 'Tyr',
    slc : 'Y',
    monoIsotopicMass : 163.06,
    avgMass: 163.18
};

aminoAcidsByName['W'] = aminoAcidsByMMass[186] = {
    name : 'Tryptophan',
    tlc : 'Trp',
    slc : 'W',
    monoIsotopicMass : 186.08,
    avgMass: 186.21
};
