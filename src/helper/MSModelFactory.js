import MSModel from './MSModel.js';
import MSEdgeFactory from './MSEdgeFactory.js';
import Util from './Util.js';

export default class MSModelFactory {
  createModelMidWeight(key) {
    if (this.modelMidWeightMap === undefined) {
      this.modelMidWeightMap = new Map();
      this.createModelName().forEach(model => {
        const id = Util.getKey3(model._edges[0].name, model._edges[1].name, model._edges[2].name);
        if (this.modelMidWeightMap.has(id)) {
          const models = this.modelMidWeightMap.get(id);
          models.push(model);
        } else this.modelMidWeightMap.set(id, [model]);
      });
    }
    return this.modelMidWeightMap.get(key);
  }

  createModelName() {
    if (this.modelNames === undefined) {
      this.modelNames = [];
      const edgeFactory = new MSEdgeFactory();

      this.modelNames.push(
        new MSModel(
          1,
          edgeFactory.createEdge('lossH2OaToc'), // 63
          edgeFactory.createEdge('lossH2OaTob'), // 46
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['c', 'b', 'aH2O'] // ['1-2', '1-3', '2-3']
        )
      );
      this.modelNames.push(
        new MSModel(
          2,
          edgeFactory.createEdge('lossNH3aToc'), // 62
          edgeFactory.createEdge('aTocORlossNH3aTob'), // 45
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['c', 'b', 'aNH3']
        )
      );
      this.modelNames.push(
        new MSModel(
          3,
          edgeFactory.createEdge('lossNH3aToc'), // 62
          edgeFactory.createEdge('lossNH3aTolossH2Ob'), // 27
          edgeFactory.createEdge('lossH2ObToc'), // 35
          ['c', 'bH2O', 'aNH3']
        )
      );

      this.modelNames.push(
        new MSModel(
          4,
          edgeFactory.createEdge('lossNH3aToc'), // 62
          edgeFactory.createEdge('aTob'), // 28
          edgeFactory.createEdge('lossNH3bToc'), // 34
          ['c', 'bNH3', 'aNH3']
        )
      );
      this.modelNames.push(
        new MSModel(
          5,
          edgeFactory.createEdge('lossH2OaTob'), // 46 *
          edgeFactory.createEdge('waterLoss'), // 18
          edgeFactory.createEdge('aTob'), // 28
          ['b', 'a', 'aH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          6,
          edgeFactory.createEdge('lossH2OaTob'), // 46 *
          edgeFactory.createEdge('aTob'), // 28
          edgeFactory.createEdge('waterLoss'), // 18
          ['b', 'bH2O', 'aH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          7,
          edgeFactory.createEdge('lossH2OaTob'), // 46
          edgeFactory.createEdge('lossH2OaTolossNH3b'), // 29
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['b', 'bNH3', 'aH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          8,
          edgeFactory.createEdge('aTocORlossNH3aTob'), // 45 *
          edgeFactory.createEdge('aTob'), // 28
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['b', 'a', 'aNH3']
        )
      );
      this.modelNames.push(
        new MSModel(
          9,
          edgeFactory.createEdge('aTocORlossNH3aTob'), // 45 *
          edgeFactory.createEdge('aTob'), // 28
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['c', 'b', 'a']
        )
      );
      this.modelNames.push(
        new MSModel(
          10,
          edgeFactory.createEdge('aTocORlossNH3aTob'), // 45 *
          edgeFactory.createEdge('aTob'), // 28
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['b', 'bNH3', 'aNH3']
        )
      );
      this.modelNames.push(
        new MSModel(
          11,
          edgeFactory.createEdge('aTocORlossNH3aTob'), // 45
          edgeFactory.createEdge('aToblossNH3'), // 11
          edgeFactory.createEdge('lossNH3bToc'), // 34
          ['c', 'bNH3', 'a']
        )
      );
      this.modelNames.push(
        new MSModel(
          12,
          edgeFactory.createEdge('aTocORlossNH3aTob'), // 45
          edgeFactory.createEdge('aToblossH2O'), // 10
          edgeFactory.createEdge('lossH2ObToc'), // 35
          ['c', 'bH2O', 'a']
        )
      );
      this.modelNames.push(
        new MSModel(
          13,
          edgeFactory.createEdge('lossH2ObToc'), // 35
          edgeFactory.createEdge('waterLoss'), // 18
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['c', 'b', 'bH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          14,
          edgeFactory.createEdge('lossNH3bToc'), // 34 *
          edgeFactory.createEdge('amoniumLoss'), // 17
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['c', 'b', 'bNH3']
        )
      );
      /*  this.modelNames.push(
        new MSModel(
          15,
          edgeFactory.createEdge('lossNH3bToc'), // 34 *
          edgeFactory.createEdge('amoniumLoss'), // 17
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['c', 'bNH3', 'b']
        )
      ); */
      this.modelNames.push(
        new MSModel(
          16,
          edgeFactory.createEdge('lossH2OaTolossNH3b'), // 29
          edgeFactory.createEdge('waterLoss'), // 18
          edgeFactory.createEdge('aToblossNH3'), // 11
          ['bNH3', 'a', 'aH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          17,
          edgeFactory.createEdge('aTob'), // 28
          edgeFactory.createEdge('aToblossNH3'), // 11
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['b', 'bNH3', 'a']
        )
      );
      this.modelNames.push(
        new MSModel(
          18,
          edgeFactory.createEdge('lossNH3aTolossH2Ob'), // 27
          edgeFactory.createEdge('amoniumLoss'), // 17
          edgeFactory.createEdge('aToblossH2O'), // 10
          ['bH2O', 'a', 'aNH3']
        )
      );
      this.modelNames.push(
        new MSModel(
          19,
          edgeFactory.createEdge('waterLoss'), // 18 *
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['a', 'aNH3', 'aH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          20,
          edgeFactory.createEdge('waterLoss'), // 18 *
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['b', 'bNH3', 'bH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          21,
          edgeFactory.createEdge('waterLoss'), // 18 *
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('amoniumLoss'), // 17
          ['y', 'yNH3', 'yH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          22,
          edgeFactory.createEdge('lossH2OyToz'), // 2
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('lossNH3yToz'), // 1
          ['z', 'yNH3', 'yH2O'] // ['1-2', '1-3', '2-3']
        )
      );
      /*      this.modelNames.push(
        new MSModel(
          23,
          edgeFactory.createEdge('lossH2OyToz'), // 2
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('lossNH3yToz'), // 1
          ['yNH3', 'yH2O', 'z'] // ['1-2', '1-3', '2-3']
        )
      ); */
      this.modelNames.push(
        new MSModel(
          24,
          edgeFactory.createEdge('amoniumLoss'), // 17
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('yToz'), // 16
          ['y', 'z', 'yNH3']
        )
      );
      this.modelNames.push(
        new MSModel(
          25,
          edgeFactory.createEdge('waterLoss'), // 18
          edgeFactory.createEdge('lossH2OyToz'), // 2
          edgeFactory.createEdge('yToz'), // 16
          ['y', 'z', 'yH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          26,
          edgeFactory.createEdge('lossH2OyTox'), // 44
          edgeFactory.createEdge('lossH2OyToz'), // 2
          edgeFactory.createEdge('zTox'), // 42
          ['x', 'z', 'yH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          27,
          edgeFactory.createEdge('lossH2OyTox'), // 44
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('lossNH3yTox'), // 43
          ['x', 'yNH3', 'yH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          28,
          edgeFactory.createEdge('zTox'), // 42
          edgeFactory.createEdge('yToz'), // 16
          edgeFactory.createEdge('yTox'), // 26
          ['x', 'y', 'z']
        )
      );
      this.modelNames.push(
        new MSModel(
          29,
          edgeFactory.createEdge('lossNH3yTox'), // 43
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('zTox'), // 42
          ['x', 'z', 'yNH3'] // ['1-2', '1-3', '2-3']
        )
      );
      /*      this.modelNames.push(
        new MSModel(
          30,
          edgeFactory.createEdge('lossNH3yTox'), // 43
          edgeFactory.createEdge('lossNH3yToz'), // 1
          edgeFactory.createEdge('zTox'), // 42
          ['x', 'yNH3', 'z'] // ['1-2', '1-3', '2-3']
        )
      ); */
      this.modelNames.push(
        new MSModel(
          31,
          edgeFactory.createEdge('lossH2OyTox'), // 44
          edgeFactory.createEdge('waterLoss'), // 18
          edgeFactory.createEdge('yTox'), // 26
          ['x', 'y', 'yH2O']
        )
      );
      this.modelNames.push(
        new MSModel(
          32,
          edgeFactory.createEdge('lossNH3yTox'), // 43
          edgeFactory.createEdge('amoniumLoss'), // 17
          edgeFactory.createEdge('yTox'), // 26
          ['x', 'y', 'yNH3']
        )
      );
    }
    return this.modelNames;
  }
}
