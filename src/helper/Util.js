export default class Util {
  static getKey(key1, key2) {
    return this.hashCode([key1, key2].sort().toString());
  }
  static avg = arr => arr.reduce((sum, c) => sum + c, 0) / arr.length;

  static hashCode = s => {
    let h = 0;
    for (let i = 0; i < s.length; i += 1)

      h = (Math.imul(31, h) + s.charCodeAt(i)) | 0;
    return h;
  };

  static getKey3(key1, key2, key3) {
    return this.hashCode([key1, key2, key3].sort().toString());
  }

  static getKey4(key1, key2, key3, key4) {
    return this.hashCode([key1, key2, key3, key4].sort().toString());
  }

  static findPosition(x1, x2, x3) {
    return (x1 + x2 + x3) / 3;
  }

  static add(map, nodeId, id) {
    if (map.has(nodeId)) {
      const arr = map.get(nodeId);
      arr.add(id);
      map.set(nodeId, arr);
    } else {
      const arr = new Set();
      arr.add(id);
      map.set(nodeId, arr);
    }
  }

  static binarySearch(ions, mz, tolerance) {
    let first = 0;
    let last = ions.length - 1;
    if (ions[first] < mz - 2 * tolerance || ions[last] > mz + 2 * tolerance) return undefined;
    if (Math.abs(ions[first].mz - mz) < tolerance) return ions[first];
    if (Math.abs(ions[last].mz - mz) < tolerance) return ions[last];
    while (first <= last) {
      const middle = Math.floor((first + last) / 2);
      if (Math.abs(ions[middle].mz - mz) < tolerance) {
        return ions[middle];
      }
      if (ions[middle].mz < mz) {
        first = middle;
      } else {
        last = middle;
      }
    }
    return undefined;
  }

  static medianIntensity(peaks) {
    if (peaks.length === 0) return 0;
    peaks.sort((a, b) => {
      return a.intensity - b.intensity;
    });
    const half = Math.floor(peaks.length / 2);
    if (peaks.length % 2) {
      return peaks[half].intensity;
    }
    return (peaks[half - 1].intensity + peaks[half].intensity) / 2.0;
  }
  slidingWindow(arr, k) {
    let next = 0;
    let value = k;
    let i = next;
    while (i < arr.length) {
      const temp = [];
      while (arr[i] < value) {
        temp.push(arr[i]);
        if (arr[i] < value - k / 2) {
          next = i + 1;
        }
        i++;
      }
      console.log(temp);
      value += k / 2;
      i = next;

    }
  }
}
