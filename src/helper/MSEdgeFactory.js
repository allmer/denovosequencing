export default class MSEdgeFactory {
  _createEdgeArray(charge) {
    if (this.edgeArray === undefined) {
      this.edgeArray = [
        { value: 63.0321, color: 'red', charge, name: 'lossH2OaToc' },
        { value: 62.0481, color: 'red', charge, name: 'lossNH3aToc' },
        { value: 46.0055, color: 'red', charge, name: 'lossH2OaTob' },
        { value: 45.0215, color: 'red', charge, name: 'aTocORlossNH3aTob' },
        { value: 43.9898, color: 'green', charge, name: 'lossH2OyTox' },
        { value: 43.0058, color: 'green', charge, name: 'lossNH3yTox' },
        { value: 41.998, color: 'green', charge, name: 'zTox' },
        { value: 35.0371, color: 'red', charge, name: 'lossH2ObToc' },
        { value: 34.0531, color: 'red', charge, name: 'lossNH3bToc' },
        { value: 28.9789, color: 'red', charge, name: 'lossH2OaTolossNH3b' },
        { value: 27.9949, color: 'red', charge, name: 'aTob' },
        { value: 27.011, color: 'red', charge, name: 'lossNH3aTolossH2Ob' },
        { value: 25.9793, color: 'green', charge, name: 'yTox' },
        { value: 18.0106, color: 'blue', charge, name: 'waterLoss' },
        { value: 17.0266, color: 'blue', charge, name: 'amoniumLoss' },
        { value: 16.0187, color: 'green', charge, name: 'yToz' },
        { value: 10.9683, color: 'red', charge, name: 'aToblossNH3' },
        { value: 9.9844, color: 'red', charge, name: 'aToblossH2O' },
        { value: 1.9918, color: 'green', charge, name: 'lossH2OyToz' },
        { value: 0.9959, color: 'yellow', charge, name: 'lossNH3yToz' }
      ];
    }
    this.edgeArray.forEach(aa => {
      aa.value /= charge;
    });
    return this.edgeArray;
  }

  createEdge(key) {
    if (this.edgeMap === undefined) {
      this.edgeMap = new Map();
      this._createEdgeArray().forEach(edge => {
        this.edgeMap.set(edge.name, edge);
      });
    }
    return this.edgeMap.get(key);
  }
}
