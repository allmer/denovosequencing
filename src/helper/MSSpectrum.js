import * as constants from './Chemistry.js';

export default class MSSpectrum {
  constructor() {
    this._precursorMass = 0.0;
    this._precursorCharge = 0.0;
    this._scanNum = -1;
    this._name = 'n/a';
    this._spectrum = [];
    this._sequence = '';
    this._support = 0;
  }

  parseMGFSection(mgfSection) {
    let first = true;
    for (let i = 0; i < mgfSection.length; i += 1) {
      if (mgfSection[i].startsWith('BEGIN')) {
      } else if (mgfSection[i].includes('=')) {
        const dta = [];
        const parts = mgfSection[i].split('=');
        dta.push(parts.shift());
        dta.push(parts.join('='));
        if (dta[0] === 'PEPMASS') this._precursorMass = parseFloat(dta[1]);
        if (dta[0] === 'CHARGE') this._precursorCharge = parseInt(dta[1], 10);
        if (dta[0] === 'TITLE') {
          this._name = dta[1];
          if (dta[1].includes('seq=')) {
            this._sequence = dta[1].match(/[^{}]+(?=\})/g)[0]; // Note sequence annotation must be before all other annotations enclosed in braces: "...seq={SEQUENCE}..."
          }
          if(dta[1].includes('sup=')) {
              this._support = Number(dta[1].match(/sup={.*}/g)[0].split(/[}{]/)[1]); //support refers to how many database search engines correctly assinged the sequence (out of ~10).
          }
        }
        if (dta[0] === 'SCANS') this._scanNum = parseInt(dta[1], 10);
      } else {
        const peak = mgfSection[i].split(/\s+/);

        if (first) {
          this._precursorMass = parseFloat(peak[0]);
          this._precursorCharge = parseInt(peak[1], 10);
          first = false;
        } else {
          this._spectrum.push({
            mz: parseFloat(peak[0]),
            intensity: parseFloat(peak[1])
          });
        }
      }
    }
  }

/**
 * Adjusts the precursor mass by a set of peaks discovered in the spectrum.
 * The peaks are discovered by the given precursor mass and the precursor mass tolerance.
 * The peaks that the method looks for are:
 *      precursor at charges 1-3
 *      precursor at charges 1-3 with water loss
 *      precursor at charges 1-3 with Ammonium loss
 * An adjustement is the weighted average of the discovered peaks.
 * An adjustment is only made if more then 3 peaks from the above list are discovered.
 * @param {Number} precursorMassTolerance the mass tolerance given in absolute Dalton.
 * @returns {Number} the adjusted precursor mass. In case of error returns NaN.
 */
  adjustPrecursorMass(precursorMassTolerance) {
      if(this.precursorMass <= 0)
          return Number.NaN;
      if(this.spectrum && this.spectrum.length <= 0)
        return Number.NaN;
      let adjCandidates = [];
      let scPM = this.precursorMass * this.precursorCharge - this.precursorCharge*constants.Hydrogen.monoIsotopicMass + constants.Hydrogen.monoIsotopicMass;
      let dcPM = (scPM + constants.Hydrogen.monoIsotopicMass) / 2;
      let tcPM = (scPM + 2*constants.Hydrogen.monoIsotopicMass) / 3;
      
      //precursor mass +1
      let peaks = this.getPeaks(scPM,precursorMassTolerance);
      if(peaks && peaks.length > 0) 
          adjCandidates.push(...peaks);
      //precursor mass +2 
      peaks = this.getPeaks(dcPM,precursorMassTolerance);
      if(peaks && peaks.length > 0) {
          peaks = peaks.map(peak => {peak.mz = peak.mz*2 - constants.Hydrogen.monoIsotopicMass; return peak;});
          adjCandidates.push(...peaks);
      }
      //precursor mass +3 
      peaks = this.getPeaks(tcPM,precursorMassTolerance);
      if(peaks && peaks.length > 0) {
          peaks = peaks.map(peak => {peak.mz = peak.mz*3 - 2*constants.Hydrogen.monoIsotopicMass; return peak;});
          adjCandidates.push(...peaks);
      }
      //NH3 loss
      peaks = this.getPeaks(scPM - constants.Amino.monoIsotopicMass,precursorMassTolerance);
      if(peaks && peaks.length > 0) {
          peaks = peaks.map(peak => {peak.mz += constants.Amino.monoIsotopicMass; return peak});
          adjCandidates.push(...peaks);
      }
      //precursor mass +2 
      peaks = this.getPeaks(dcPM - constants.Amino.monoIsotopicMass/2,precursorMassTolerance);
      if(peaks && peaks.length > 0) {
          peaks = peaks.map(peak => {peak.mz = peak.mz*2 - constants.Hydrogen.monoIsotopicMass + constants.Amino.monoIsotopicMass; return peak;});
          adjCandidates.push(...peaks);
      }
      //precursor mass +3 
      peaks = this.getPeaks(tcPM - constants.Amino.monoIsotopicMass/3,precursorMassTolerance);
      if(peaks && peaks.length > 0)  {
          peaks = peaks.map(peak => {peak.mz = peak.mz*3 - constants.Hydrogen.monoIsotopicMass + constants.Amino.monoIsotopicMass; return peak;});
          adjCandidates.push(...peaks);
      }
      //water loss
      peaks = this.getPeaks(scPM - constants.Water.monoIsotopicMass,precursorMassTolerance);
      if(peaks && peaks.length > 0)   {
          peaks = peaks.map(peak => {peak.mz = peak.mz + constants.Amino.monoIsotopicMass; return peak;});
          adjCandidates.push(...peaks);
      }
      //precursor mass +2 
      peaks = this.getPeaks(dcPM - constants.Water.monoIsotopicMass/2,precursorMassTolerance);
      if(peaks && peaks.length > 0)  {
          peaks = peaks.map(peak => {peak.mz = peak.mz*2 - constants.Hydrogen.monoIsotopicMass + constants.Water.monoIsotopicMass; return peak;});
          adjCandidates.push(...peaks);
      }
      //precursor mass +3 
      peaks = this.getPeaks(tcPM - constants.Water.monoIsotopicMass/3,precursorMassTolerance);
      if(peaks && peaks.length > 0)   {
          peaks = peaks.map(peak => {peak.mz = peak.mz*3 - constants.Hydrogen.monoIsotopicMass + constants.Water.monoIsotopicMass; return peak;});
          adjCandidates.push(...peaks);
      }
      if(adjCandidates.length < 3)
          return Number.NaN;
      console.log(adjCandidates);
      let summedIntensity = adjCandidates.reduce((sum, b) => sum + (b.intensity || 0), 0);
      console.log(summedIntensity);
      let newPM = adjCandidates.reduce((sum, b) => sum + (b.intensity/summedIntensity * b.mz || 0), 0);
      console.log(newPM);
      return newPM;
  };

/**
 * Adjusts the precursor mass by a set of peaks discovered in the spectrum.
 * for each peak all peaks within PM-10 to PM are discovered and added up.
 * The sum is placed into a map, indexed by the integer part of the sum.
 * @param {Number} tolerance the maximum mass tolerance from the precursor in absolute Dalton (default: 10).
 * @param {Number} maxCharge the maximum charge to take into account. If 0 trust the charge provided (default: 0).
 * @returns {Number} the adjusted precursor mass. In case of error returns NaN.
 */
  adjustPrecursorMassByComplementarity(tolerance = 10, maxCharge = 0) {
      let newPM = 0;
      let sumIdx = new Map();
      for(let i=0; i<this.spectrum.length; i++) {
          let candidates = this.getPeaks(this.spectrum[i].mz-tolerance,this.spectrum[i]);
          for(let c=0; c<candidates.length; c++) {
              let cPmz = candidates[c].mz+this.spectrum[i].mz;
              let cAbu = candidates[c].intensity+this.spectrum[i].intensity;
              let cPeak = {"mz":cPmz,"intensity":cAbu};
              let idx = Math.round(cPmz);
              if(!sumIdx.has(idx)) {
                  let cArr = [];
                  cArr.push(cPeak);
                  sumIdx.set(idx,cArr);
              } else {
                  let cArr = sumIdx.get[idx];
                  cArr.push(cPeak);
                  sumIdx.set(idx,cArr);
              }
          }
      }
      let maxCt = -1;
      let maxIdx = -1;
      let keys = sumIdx.keys();
      for(let k=0; k<keys.length; k++) {
          let tmp = sumIdx.get(keys[k]);
          if(tmp.length > maxCt) {
              maxCt = tmp.length;
              maxIdx = keys[k];
          }
      }
      let pmArr = sumIdx.get(maxIdx);
      let summedIntensity = pmArr.reduce((sum, b) => sum + (b.intensity || 0), 0);
      let newPM = pmArr.reduce((sum, b) => sum + (b.intensity/summedIntensity * b.mz || 0), 0);
      return newPM;
  };

/**
 * When all peaks within the window +/- tol are needed please use getPeaks.
 * If the highest intensity is searched for use getMostIntense
 * if the closest peak is interesting use getClosest. 
 * Selects all the peaks that fall within +/- tol(erance) of the given mz (mass to charge ratio).
 * @param {Number} mz Mass to charge ratio to look for.
 * @param {Number} tol Tolerance window to apply when looking for peaks (|peak.mz - mz| < tol).
 * @returns {Array} All peaks that fall into the desired range or an empty Array. peak['mz'] and peak['intensity] exist.   
 */
  getPeaks = (mz, tol) => { 
    return this.spectrum.filter(peak => Math.abs(mz - peak.mz) < tol);
  };

/**
 * When all peaks within the window +/- tol are needed please use getPeaks.
 * If the highest intensity is searched for use getMostIntense
 * if the closest peak is interesting use getClosest.
 * @param {Number} mz Mass to charge ratio to look for.
 * @param {Number} tol Tolerance window to apply when looking for peaks (|peak.mz - mz| < tol).
 * @returns null if no result otherwise the peak closest to the reference mz.   
 */
  getClosest(mz, tol) {
      let peaks = this.getPeaks(mz,tol);
      if(!peaks || peaks.length <= 0)
          return null;
      for(let i=0; i<peaks.length; i++) {
          peaks[i]['dist'] = Math.abs(peaks[i].mz - mz);
      }
      peaks.sort(function(a, b) {
        return a.dist - b.dist;
      }); 
      return peaks[0];
  }
  
/**
 * When all peaks within the window +/- tol are needed please use getPeaks.
 * If the highest intensity is searched for use getMostIntense
 * if the closest peak is interesting use getClosest.
 * @param {Number} mz Mass to charge ratio to look for.
 * @param {Number} tol Tolerance window to apply when looking for peaks (|peak.mz - mz| < tol).
 * @returns null if no result otherwise the peak with the highest intensity in the window.   
 */  
  getMostIntense(mz, tol) {
      let peaks = this.getPeaks(mz,tol);
      if(!peaks || peaks.length <= 0)
          return null;
      peaks.sort(function(a, b) {
        return b.intensity - a.intensity;
      }); 
      return peaks[0];
  }

/**
 * Deprecated!
 * Potentially dangerous since in a real spectrum many peaks can be within the tolerance and potentially contains selects the wrong one.
 * If returning only true and false, this method is acceptable.
 */
/*  contains = (mz, tol) => {
    return this.spectrum.find(peak => Math.abs(mz - peak.mz) < tol);
  };*/

  getPrecursorMass() {
    return this.precursorMass * this.precursorCharge - this.precursorCharge + 1;
  }

  get precursorMass() {
    return this._precursorMass;
  }

  set precursorMass(value) {
    this._precursorMass = value;
  }

  get precursorCharge() {
    return this._precursorCharge;
  }

  set precursorCharge(value) {
    this._precursorCharge = value;
  }

  get scanNumber() {
    return this._scanNum;
  }

  set scanNumber(value) {
    this._scanNum = value;
  }

    get charge() {
        return this._precursorCharge;
    }

    set charge(value) {
        this._precursorCharge = value;
    }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get sequence() {
    return this._sequence;
  }

  set sequence(value) {
    this._sequence = value;
  }
  
  get support() {
    return this._support;
  }
  
  get spectrum() {
    return this._spectrum;
  }
  set spectrum(value){
    this._spectrum = value;
  }
}
