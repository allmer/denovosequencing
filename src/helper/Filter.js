export default class Filter{

    chunkArray(myArray, chunk_size){
        let index = 0;
        const arrayLength = myArray.length;
        const tempArray = [];

        for (index = 0; index < arrayLength; index += chunk_size) {
            const myChunk = myArray.slice(index, index+chunk_size);
            tempArray.push(myChunk);
        }

        return tempArray;
    }
    
    windowSliding(bIons, window, step, kMax) {
        const sortedBIons = bIons.sort((a, b) => {
            return a.mz - b.mz;
        });
        const result = new Set();
        let currentIndex = 0;
        for (let currentStep = 0; currentStep < this.pm; currentStep += step) {
            const temp = [];
            for (
                let i = currentIndex;
                i < sortedBIons.length && sortedBIons[i].mz < window + currentStep;
                i++
            ) {
                if (sortedBIons[i].mz < currentStep + step) {
                    currentIndex = i + 1;
                }
                temp.push(sortedBIons[i]);
            }
            temp.sort((a, b) => {
                return b.intensity - a.intensity;
            });

            for (let i = 0; i < temp.length && i < kMax; i += 1) {
                if (temp[i] !== undefined) {
                    result.add(temp[i]);
                }
            }
        }
        return Array.from(result).sort((a, b) => {
            return a.mz - b.mz;
        });
    }


    static normalizeIntensityByCat(peaks){
        const sortedSpectrum = peaks.sort((a, b) => {
            return a.intensity - b.intensity;
        });
        const min = sortedSpectrum.slice(0, 9).reduce((sum, c) => sum +  c.intensity, 0) / 10;
        const max = sortedSpectrum.slice(sortedSpectrum.length - 10, sortedSpectrum.length).reduce((sum, c) => sum +  c.intensity, 0) / 10;

        let minIndex = 0;
        while(sortedSpectrum[minIndex].intensity < min){
            sortedSpectrum[minIndex].intensity = 1;
            minIndex += 1;
        }

        const chunk_size = Math.floor((sortedSpectrum.length - minIndex) / 8);
        let value = 2;
        for (let index = minIndex; index < sortedSpectrum.length; index += chunk_size) {
            for(let i = index; i < index+chunk_size && i< sortedSpectrum.length; i += 1){
                sortedSpectrum[i].intensity = value;
            }
            value += 1;
        }
        return sortedSpectrum;
    }

    normalizeIntensityBySD(msSpectrum){
        const bucketSize = 10;

        const mean = msSpectrum.spectrum.reduce((a, b) => {
            return a.intensity + b.intensity;
        }) / msSpectrum.spectrum.length;

        const std = Math.sqrt( msSpectrum.spectrum.reduce((sq, n) => {
            return sq + Math.pow(n.intensity - mean, 2);
        }, 0) / (msSpectrum.spectrum.length - 1));

        const minValue = mean - 2 * std;
        const maxValue = mean + 2 * std;

        const spectrum = [];
        for (let i = 0; i < msSpectrum.spectrum.length; i += 1){
            const intensity = msSpectrum.spectrum[i].intensity;
            if (intensity >= minValue && intensity <= maxValue){
                msSpectrum.spectrum[i].intensity = Math.floor(( intensity - minValue) / bucketSize) + 1;
            }
        }
        return msSpectrum;
    }
    
    static filterByMinIntensity(peaks,cutoff) {
        const filtered = [];
        for (let i = 0; i < peaks.length; i++) {
            if (peaks[i].intensity >= cutoff) {
                filtered.push(peaks[i]);
            }
        }
        //msSpectrum.spectrum = filtered; //dangerous side effects expected here.
        return filtered;
    }

    /**
     * Removes the bottom filterPercent percent of the peaks from a spectrum.
     * @param peaks the array with the peaks. Format:[{mz:num,intensity:num},{..},..].
     * @param filterPercent percentage given in the interval 0 .. 1 with 0.3 = 30%.
     * @returns the filtered array of the peaks (can be empty).
     */
    static filterBotXPercent(peaks,filterPercent) {
        const filtered = [];
        peaks.sort((l,r) => r.intensity - l.intensity); //sort array by intensity (DEC).
        let good = Math.floor(peaks.length * (1-filterPercent));
        for (let i = 0; i < good; i++) {
            filtered.push(peaks[i]);
        }
        filtered.sort((l,r) => l.mz - r.mz);
        peaks.sort((l,r) => l.mz - r.mz); //make sure to not cause side effects and sort by mz again (ASC).
        return filtered;
    }
    
    /**
     * Removes all the peaks per window that are not as intense as the best num2retain peaks.
     * @param peaks the array with the peaks. Format:[{mz:num,intensity:num},{..},..].
     * @param num2retain the number of peaks per window to keep (all others will be discarded).
     * @param numWin the number of windows to consider.
     * @returns the filtered array of the peaks (can be empty).
     */    
    static filterBestNperWindow(peaks,num2retain,numWin) {
        const filtered = [];
        const winSize = (peaks[peaks.length-1].mz - peaks[0].mz)/numWin + 0.1;
        let border = winSize + peaks[0].mz;
        let tPeaks = [];
        for(let i=0; i<peaks.length; i++) {
            if(peaks[i].mz > border) {
                tPeaks.sort((l,r) => r.intensity - l.intensity);
                for(let p=0; p<num2retain && p<tPeaks.length;p++) 
                    filtered.push(tPeaks[p]);
                border += winSize;
                tPeaks = [];
                i--;            //Perhaps a different loop construction could avoid manipulation of the loop variable.
            } else {
                tPeaks.push(peaks[i]);
            }
        }
        tPeaks.sort((l,r) => r.intensity - l.intensity);
        for(let p=0; p<num2retain && p<tPeaks.length;p++) 
            filtered.push(tPeaks[p]);
        return filtered;        
    }
    
    /**
     * Detects isotopic paterns and collapses them to the non-isotopic base peak.
     * @returns the filtered spectrum with less/without isotopic peaks.
     */
    static isotopeCollapse(peaks) {
        /* TODO */
    }
    
}