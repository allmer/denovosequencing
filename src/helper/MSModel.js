export default class MSModel {
  constructor(id, edge1, edge2, edge3, nodes) {
    this._id = id;
    this._edges = [edge1, edge2, edge3];
    this._nodes = nodes;
  }

  contains(edge) {
    let found = false;
    for (let i = 0; i < this._edges.length; i += 1) {
      if (this._edges[i].name === edge.data().name) {
        found = true;
        break;
      }
    }
    return found;
  }

  getNodeNames() {
    return this._nodes;
  }

  get id() {
    return this._id;
  }
}
