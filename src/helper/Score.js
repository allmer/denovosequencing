export default class Score{
    static longest = (x, y) => (x.length > y.length) ? x : y;

    static lcs = (str1, str2) => {
        if (!str1.length || !str2.length) { return ''; }

        const [x, ...xs] = str1;
        const [y, ...ys] = str2;

        return (x === y) ? (x + this.lcs(xs, ys)) : this.longest(this.lcs(str1, ys), this.lcs(xs, str2));
    };
    static editDistDP(str1,str2) {
        const dp = Array.from(Array(str1.length + 1), () => new Array(str2.length + 1));
        for (let i = 0; i <= str1.length; i += 1) {
            for (let j = 0; j <= str2.length; j += 1) {
                if (i === 0) {
                    dp[i][j] = j; // Min. operations = j
                } else if (j === 0) {
                    dp[i][j] = i; // Min. operations = i
                } else if (str1[i - 1] === str2[j - 1]) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] =
                        1 +
                        Math.min(
                            dp[i][j - 1], // Insert
                            dp[i - 1][j], // Remove
                            dp[i - 1][j - 1] // Replace
                        );
                }
            }
        }
        return dp[str1.length][str2.length];
    }
}