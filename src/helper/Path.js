import PriorityQueue from "./PriorityQueue.js";
import Graph from "../graph/Graph.js";
import Queue from "./Queue.js";
import clonedeep from 'lodash.clonedeep';

export default class Path{
    static topologicalSortHelper = (bIonGraph, nodeId, visited, stack) => {
        visited.add(nodeId);
        // console.log(`${nodeId} - ${this.AdjList.get(nodeId)}`);
        if (bIonGraph.AdjList.has(nodeId)) {
            bIonGraph.AdjList.get(nodeId).forEach(nId => {
                if (!visited.has(nId)) {
                    this.topologicalSortHelper(bIonGraph, nId, visited, stack);
                }
            });
        }
        stack.push(nodeId);
    };
    static isEqualPath(p1, p2) {
        if (p1.length !== p2.length) {
            return false;
        }
        return p1.every((elm, index) => elm === p2[index]);
    }



static longestPath = (graph, start, end) => {
        const stack = [];
        const cost = {};
        const visited = new Set();
        const comesFrom = new Map();
        // const bucketCount = 20;
        // const bucketIntensity = new Bucket(bucketCount, 0, this.pm);

        // console.log('-------------------');
        graph.AdjList.forEach((v, node) => {
            if (!visited.has(node)) {
                this.topologicalSortHelper(graph, node, visited, stack);
            }
        });
        // console.log('--------------');
        let min = Number.MAX_SAFE_INTEGER;
        let max = Number.MIN_SAFE_INTEGER;
        graph.AdjList.forEach((values, nodeId) => {
            cost[nodeId] = Number.MIN_SAFE_INTEGER;
            // const v = graph.nodes.get(nodeId).support;
            // console.log(`${nodeId}:${v}`);
            // min = v < min ? v : min;
            // max = v > max ? v : max;
        });
        // const bucketSupport = new Bucket(bucketCount, min, max);
        cost[start] = 0;
        while (stack.length > 0) {
            const uId = stack.pop();

            if (graph.AdjList.has(uId)) {
                graph.AdjList.get(uId).forEach(vId => {
                    const node = graph.nodes.get(uId);
                    // console.log(vId);
                    /*          const edgeWeight = this.getEdgeWeight(vId, uId);
                    const normalizedSupport = bucketSupport.getBucketIdxForValue(node.support) / bucketCount;
                    const normalizedIntensity =
                      bucketIntensity.getBucketIdxForValue(node.intensity) / bucketCount;
                    const dist = 0.5 * normalizedIntensity + 0.3 * normalizedSupport + 0.2 * edgeWeight; */
                    if (cost[vId] < cost[uId] + node.intensity) {
                        cost[vId] = cost[uId] + node.intensity;
                        comesFrom.set(vId, uId);
                    }
                });
            }
        }
        const path = [];
        if (comesFrom.has(end)) {
            path.push(end);
            while (path[0] !== undefined) {
                path.unshift(comesFrom.get(path[0]));
            }
        }
        path.shift();

        return [cost, path];
    };

/*
    static findYenKLP = (graph, source, sink, Kth) => {
        const [cost, path] = this.longestPath(graph, source, sink)
        const A = [{ cost: cost[sink], path: path }];
        const B = new PriorityQueue((a, b) => a.cost > b.cost);

        let graph_ = clonedeep(graph);
        for (let k = 1; k < Kth; k += 1) {
            for (let i = 0; i <= A[k - 1].path.length - 2; i += 1) {
                const spurNode = A[k - 1].path[i];
                const rootPath = A[k - 1].path.slice(0, i + 1);

                A.forEach(a =>{
                    if (this.isEqualPath(rootPath, a.path.slice(0, i + 1))) {
                        graph_.removeEdge(a.path[i], a.path[i + 1]);
                    }
                });

                for (let n = 0; n < rootPath.length - 1; n += 1) {
                    graph_.removeNode(rootPath[n]);
                }

                const [spurCost, spurPath] = this.longestPath(spurNode, sink);

                const totalPath = rootPath.concat(spurPath);

                B.push({ cost: spurCost[sink] + cost[spurNode], path: totalPath });

                graph_ = clonedeep(graph);

            }

            if (B.isEmpty()) break;
            A[k] = B.pop();
        }
        return A;
    };
*/

    static findYenKLP = (graph, source, sink, Kth) => {
        let graph_ = clonedeep(graph);
        const [longestCost, longestPath] = this.longestPath(graph_, source, sink);
        const A = [{  path: longestPath, cost: longestCost[sink] }];
        const B = new PriorityQueue();

        for (let k = 1; k < Kth; k += 1) {
            for (let i = 1; i <= A[k - 1].path.length - 2; i += 1) {
                const spurNode = A[k - 1].path[i];
                const rootPath = A[k - 1].path.slice(0, i + 1);
                for (let p = 0; p < A.length; p += 1) {
                    if (this.isEqualPath(rootPath, A[p].path.slice(0, i + 1))) {
                        if (graph_.AdjList.get(A[p].path[i]).includes(A[p].path[i + 1])) {
                            graph_.removeEdge(A[p].path[i], A[p].path[i + 1]);
                        }
                    }
                }
                for (let n = 0; n < rootPath.length - 1; n += 1) {
                    graph_.removeNode(rootPath[n]);
                }

                const [spurCost, spurPath] = this.longestPath(graph_, spurNode, sink);

                if (spurCost[sink] > 0) {
                    if (rootPath[rootPath.length - 1] === spurPath[0]) {
                        const totalPath = rootPath.concat(spurPath.slice(1));
                        B.enqueue(totalPath ,  spurCost[sink] + longestCost[spurNode]);
                    }
                }
                graph_ = clonedeep(graph);

            }
            if (B.isEmpty()) break;
            A[k] = B.dequeue();
        }
        return A;
    };


    static findPathsByBFS(graph, src, dst, k){
        const queue = new Queue();
        const results = new PriorityQueue((a, b) => a[1] > b[1]);
        const el = [[], 0];
        el[0].push(src);
        queue.enqueue(el);
        while (!queue.isEmpty()){
            const el = queue.dequeue();
            const last = el[0][el[0].length - 1];
            if (last === dst){
                results.push(el)
            }
            graph.connectedNodes(last).forEach(v=> {
                if(!el[0].includes(v)){
                    let newEl = [Array.from(el[0]),el[1]];
                    newEl[0].push(v);
                    newEl[1] += graph.nodes.get(v).intensity;
                    queue.enqueue(newEl);
                }
            });
        }
        const paths = [];
        let index = 0;
        while (!results.isEmpty() && index < k) {
            paths.push(results.pop()[0])
            index++;
        }
        return paths;
    }

}