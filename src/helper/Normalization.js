export default class Normalization{
    static normalizeIntensityByCat(msSpectrum){
        const sortedSpectrum = msSpectrum.spectrum.sort((a, b) => {
            return a.intensity - b.intensity;
        });
        const min = sortedSpectrum.slice(0, 9).reduce((sum, c) => sum +  c.intensity, 0) / 10;
        const max = sortedSpectrum.slice(sortedSpectrum.length - 10, sortedSpectrum.length).reduce((sum, c) => sum +  c.intensity, 0) / 10;

        let minIndex = 0;
        while(sortedSpectrum[minIndex].intensity < min){
            sortedSpectrum[minIndex].intensity = 1;
            minIndex += 1;
        }

        const chunk_size = Math.floor((sortedSpectrum.length - minIndex) / 8);
        let value = 2;
        for (let index = minIndex; index < sortedSpectrum.length; index += chunk_size) {
            for(let i = index; i < index+chunk_size && i< sortedSpectrum.length; i += 1){
                sortedSpectrum[i].intensity = value;
            }
            value += 1;
        }
        msSpectrum.spectrum = sortedSpectrum;
        return msSpectrum;
    }

    normalizeIntensityBySD(msSpectrum){
        const bucketSize = 10;

        const mean = msSpectrum.spectrum.reduce((a, b) => {
            return a.intensity + b.intensity;
        }) / msSpectrum.spectrum.length;

        const std = Math.sqrt( msSpectrum.spectrum.reduce((sq, n) => {
            return sq + Math.pow(n.intensity - mean, 2);
        }, 0) / (msSpectrum.spectrum.length - 1));

        const minValue = mean - 2 * std;
        const maxValue = mean + 2 * std;

        const spectrum = [];
        for (let i = 0; i < msSpectrum.spectrum.length; i += 1){
            const intensity = msSpectrum.spectrum[i].intensity;
            if (intensity >= minValue && intensity <= maxValue){
                msSpectrum.spectrum[i].intensity = Math.floor(( intensity - minValue) / bucketSize) + 1;
            }
        }
        return msSpectrum;
    }
}