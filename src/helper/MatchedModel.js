export default class MatchedModel {
  constructor(id, mz, intensity, model, nodes, edges, x, y) {
    this.id = id;
    this.mz = mz;
    this.intensity = intensity;
    this.model = model;
    this.nodes = nodes;
    this.edges = edges;
    this.x = x;
    this.y = y;
  }
}