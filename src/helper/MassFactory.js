export default class MassFactory {
  constructor() {
    this._MONOISOTOPIC = 0.0;
    this._AVERAGE = 1.0;
    this._H = 1.00783;
    this._O = 15.9994;
    this._OH = 17.00723;
    this._H2O = 18.01506;
    this._N = 14.0067;
    this._NH = 15.01453;
    this._NH2 = 16.02236;
    this._NH3 = 17.030189999999997;
    this._C = 12.011;
    this._CO = 28.010399999999997;
  }

  get MONOISOTOPIC() {
    return this._MONOISOTOPIC;
  }

  set MONOISOTOPIC(value) {
    this._MONOISOTOPIC = value;
  }

  get AVERAGE() {
    return this._AVERAGE;
  }

  set AVERAGE(value) {
    this._AVERAGE = value;
  }

  get H() {
    return this._H;
  }

  set H(value) {
    this._H = value;
  }

  get O() {
    return this._O;
  }

  set O(value) {
    this._O = value;
  }

  get OH() {
    return this._OH;
  }

  set OH(value) {
    this._OH = value;
  }

  get H2O() {
    return this._H2O;
  }

  set H2O(value) {
    this._H2O = value;
  }

  get N() {
    return this._N;
  }

  set N(value) {
    this._N = value;
  }

  get NH() {
    return this._NH;
  }

  set NH(value) {
    this._NH = value;
  }

  get NH2() {
    return this._NH2;
  }

  set NH2(value) {
    this._NH2 = value;
  }

  get NH3() {
    return this._NH3;
  }

  set NH3(value) {
    this._NH3 = value;
  }

  get C() {
    return this._C;
  }

  set C(value) {
    this._C = value;
  }

  get CO() {
    return this._CO;
  }

  set CO(value) {
    this._CO = value;
  }
}
