
export default class PriorityQueue {

  constructor()
  {
    this.items = [];
  }

  enqueue(path, cost)
  {
    let contain = false;
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].cost < cost) {
        this.items.splice(i, 0, {path: path, cost:cost });
        contain = true;
        break;
      }
    }

    if (!contain) {
      this.items.push({path: path, cost:cost });
    }
  }

  dequeue()
  {
    if (this.isEmpty())
      return "Underflow";
    return this.items.shift();
  }

  front()
  {
    if (this.isEmpty())
      return "No elements in Queue";
    return this.items[0];
  }

  rear()
  {
    if (this.isEmpty())
      return "No elements in Queue";
    return this.items[this.items.length - 1];
  }
// isEmpty function
  isEmpty()
  {
    // return true if the queue is empty.
    return this.items.length === 0;
  }
} 
