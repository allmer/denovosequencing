import fs from 'fs';
import MSEdgeFactory from './MSEdgeFactory.js';
import MSSpectrum from './MSSpectrum.js';
import GraphSpectrum from './GraphSpectrum.js';
import Model from '../Model.js';
import AminoAcidEdgeFactory from './AminoAcidEdgeFactory.js';
import Util from './Util.js';
import Node from '../graph/Node.js';

class Statistics {

/*
    validPath = (coloredEdgesTolerance, creatingBIonsTolerance, mergeTolerance) => {
    const model = new Model();
    const mSEdgeFactory = new MSEdgeFactory();
    const edgeNames = mSEdgeFactory._createEdgeArray();
    const text = fs.readFileSync('../../data/3spectra.mgf', 'utf8');
    const spectra = text.split('END IONS');
    spectra.pop(); // remove empty element.
    for (let s = 0; s < spectra.length; s += 1) {
      spectra[s] = spectra[s].trim();
      spectra[s] = spectra[s].match(/[^\r\n]+/g);
      const msSpectrum = new MSSpectrum();
      msSpectrum.parseMGFSection(spectra[s]);
      const gms = new GraphSpectrum(msSpectrum, coloredEdgesTolerance);
      model.pm = msSpectrum.getPrecursorMass();
      const nodes = gms.nodes;
      const edges = [];
      model.graph.addNodes(nodes);
      edgeNames.forEach(edge => {
        const tmp = gms.getEdges(edge.name);
        if (tmp !== undefined) {
          edges.push(...tmp);
          model.graph.addEdges(edges);
        }
      });
      model.updateGraph(nodes, edges);
      const bIons = model.createBIons(creatingBIonsTolerance);
      // const filteredBIons = model.mergeBIons(bIons);
      const filteredBIons = model.mergeBIons(bIons, mergeTolerance);
      const mergedBIons = model.windowSliding(filteredBIons, 50, 25, 4);

      let start = mergedBIons.find(bIon => Math.abs(1.0 - bIon.mz) < mergeTolerance);
      if (start === undefined) {
        start = new Node(1.0, 1.0, 1.0, 1, 0, 0);
        mergedBIons.push(start);
      }
      let end = mergedBIons.find(bIon => Math.abs(model.pm - 18.0106 - bIon.mz) < mergeTolerance);
      if (end === undefined) {
        const medianIntensity = Util.medianIntensity(mergedBIons);
        end = new Node(model.pm - 18.0098, model.pm - 18.0098, medianIntensity, 1, 0, 0);
        mergedBIons.push(end);
      }
      const edge = model.createAminoAcidEdges(mergedBIons);
      model.updateGraph(mergedBIons, edge);
      const expectedPath = this.checkBIons(mergedBIons, msSpectrum.sequence, mergeTolerance);

      // const end = Util.binarySearch(mergedBIons, model.pm - 18.0106, tolerance);
      const [cost, winningPath] = model.longestPath(start.mz, end.mz);
      // const A = model.findYenKLP(start.id, end.id, 3, tolerance);
      // console.log(winningPath);
      let sequence = '';
      const aminoAcidEdgeFactory = new AminoAcidEdgeFactory();
      for (let i = 0; i < winningPath.length - 1; i += 1) {
        sequence += aminoAcidEdgeFactory.findAminoAcid(
          Math.abs(winningPath[i] - winningPath[i + 1]),
          tolerance
        ).oneLetterCode;
      }
      // console.log(`expected path: ${pathFound} `);

      console.log(`expected sequence: ${msSpectrum.sequence} - ${sequence}`);

      // console.log(`(total intensity: ${totalIntensity}) found path(mz, intensity):`);
      // console.log(found);

      /!*      const editDistance = Statistics.editDistDP(msSpectrum.sequence, sequence);
      console.log(
        `expected sequence: ${msSpectrum.sequence} - found sequence: ${sequence}  edit : ${editDistance} expected bIons: ${expected} - actual bIons: ${actual} - found: ${validPath}`
      ); *!/
    }
  };*/

  createArtificialSpectrum = (sequence, pm) => {
    const data = [];
    const aAFactory = new AminoAcidEdgeFactory();
    let AAs = 0;
    let ion = 0;
    for (let i = 0; i < sequence.length; i += 1) {
      const aA = aAFactory.createEdge(sequence[i]);
      AAs += aA.monoisotopicMass;

      ion = AAs - 26.9871; // a
      data.push({ mz: ion, name: 'a' });

      ion = AAs + 1.0078; // b
      data.push({ mz: ion, name: 'b' });

      ion = AAs + 18.0344; // c
      data.push({ mz: ion, name: 'c' });

      ion = pm - AAs + 25.9793; // x
      data.push({ mz: ion, name: 'c' });

      ion = pm - AAs; // y
      data.push({ mz: ion, name: 'y' });

      ion = pm - AAs - 17.0242; // z
      data.push({ mz: ion, name: 'z' });
    }
    return data;
  };

  checkBIons = (bIons, sequence, tolerance) => {
    const aAFactory = new AminoAcidEdgeFactory();
    let AAs = 0;
    const artificialBIons = [1.0];
    // let str = '1.0  ';
    for (let i = 0; i < sequence.length; i += 1) {
      const aA = aAFactory.createEdge(sequence[i]);
      AAs += aA.monoisotopicMass;
      artificialBIons.push(AAs + 1.0078); // b
      // str += `${AAs + 1.0078}  `;
    }
    // console.log('expected b-ions', str);
    const found = [];
    for (let i = 0; i < artificialBIons.length; i += 1) {
      const bIon = bIons.find(node => Math.abs(artificialBIons[i] - node.mz) < tolerance);
      found.push(bIon);
    }
    // console.log(`(total intensity: ${totalIntensity}) b-ions(mz, intensity):`);
    // console.log(found);
    return found;
  };
}

