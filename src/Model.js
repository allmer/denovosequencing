import MSSpectrum from './helper/MSSpectrum.js';
import GraphSpectrum from './helper/GraphSpectrum.js';
import Util from './helper/Util.js';
import MSModelFactory from './helper/MSModelFactory.js';
import Graph from './graph/Graph.js';
import Node from './graph/Node.js';
import Edge from './graph/Edge.js';
import AminoAcidEdgeFactory from './helper/AminoAcidEdgeFactory.js';
import fs from "fs";
import Score from "./helper/Score.js";
import Path from "./helper/Path.js";
import Filter from "./helper/Filter.js";

export default class Model {
  static readInput(parameters){
    const msSpectrum = new MSSpectrum();
    msSpectrum.parseMGFSection(parameters.spectrum);
    parameters.pm = msSpectrum.getPrecursorMass();
    parameters.scanID = msSpectrum.scanNumber;
    parameters.expSeq = msSpectrum.sequence;
    parameters.peaks = msSpectrum.spectrum;
    delete parameters.spectrum;
    return parameters;
  }
  static writeOutput(parameters){
    const output = {};
    output.settingsID = parameters.settingsID;
    output.scanID = parameters.scanID;
    output.expSeq = parameters.expSeq;
    output.foundSeq = parameters.foundSeq;
    output.editDist = parameters.editDist;
    output.correct = parameters.correct;
    return output;

  }
  static normalizeIntensity(parameters){
    parameters.peaks  = Filter.normalizeIntensityByCat(parameters.peaks);
    return parameters;
  }

  static filterSpectrum(parameters) {
    parameters.peaks = Filter.filterByMinIntensity(parameters.peaks,parameters.cutoff);
    return parameters;
  }

  static createIonGraph(parameters) {
    const gms = new GraphSpectrum(parameters.peaks, parameters.createIonTolerance, parameters.pm);
    const nodes = gms.nodes;
    const edges = [];
    parameters.edgeNames.forEach(edge => {
      const tmp = gms.getEdges(edge);
      if (tmp !== undefined) {
        edges.push(...tmp);
      }
    });
    const graph = new Graph();
    graph.add(nodes, edges);
    parameters.ionGraph = graph;
    delete parameters.peaks;
    return parameters;
  }
  static filterIonGraphNodes(parameters){
    return parameters;
  }
  static filterIonGraphEdges(parameters){
    return parameters;
  }
  static filterModels(parameters){
    return parameters;
  }
  static mergeBIons(parameters){
    return parameters;
  }
  static filterBIons(parameters){
    return parameters;
  }
  static filterEdges(parameters) {
    return parameters;
  }
  static scorePaths(parameters) {
    return parameters;
  }
  static rankPaths(parameters) {
    return parameters;
  }

  static findPaths(parameters){
    const start = Array.from(parameters.bIonGraph.nodes.values()).find(node => Math.abs(1.0 - node.mz) < parameters.findPathTolerance).mz;
    const end = Array.from(parameters.bIonGraph.nodes.values()).find(node => Math.abs(parameters.pm - 18.0106 - node.mz) < parameters.findPathTolerance).mz;
    const results = Path.findYenKLP(parameters.bIonGraph, start, end, parameters.k);
    const seq = [];
    for (let k = 0; k< results.length; k+=1){
      let sequence = '';
      const aminoAcidEdgeFactory = new AminoAcidEdgeFactory();
      for (let i = 0; i < results[k].path.length - 1; i += 1) {
        sequence += aminoAcidEdgeFactory.findAminoAcid(
            Math.abs(results[k].path[i + 1] - results[k].path[i]),
            parameters.findPathTolerance
        ).oneLetterCode;
      }
      seq.push(sequence);
    }
    parameters.foundSeq = seq;
    return parameters;
  }

/*
  findPath = (parameters) =>{
    const start = Array.from(parameters.bIonGraph.nodes.values()).find(node => Math.abs(1.0 - node.mz) < parameters.findPathTolerance).mz;
    const end = Array.from(parameters.bIonGraph.nodes.values()).find(node => Math.abs(parameters.pm - 18.0106 - node.mz) < parameters.findPathTolerance).mz;
    const [cost, path] = Path.longestPath(parameters.bIonGraph, start, end);
    let sequence = '';
    const aminoAcidEdgeFactory = new AminoAcidEdgeFactory();
    for (let i = 0; i < path.length - 1; i += 1) {
      sequence += aminoAcidEdgeFactory.findAminoAcid(
          Math.abs(path[i] - path[i + 1]),
          parameters.findPathTolerance
      ).oneLetterCode;
    }
    delete parameters.pm;
    delete parameters.bIonGraph;
    parameters.foundSeq = sequence;
    return parameters;
  }
*/


  static lcsScorer = (parameters) =>{
    const str1 = parameters.sequence;
    const str2 = parameters.expSeq;
    const result = Score.lcs(str1,str2);
    parameters.lcsScorer = result.length;
    return parameters;
  }

  static editDistScorer(parameters){
    const foundSeq = parameters.foundSeq;
    const expSeq = parameters.expSeq;
    let editDist = 0;
    foundSeq.forEach( seq =>{
      editDist += Score.editDistDP(seq,expSeq) / foundSeq.length
    });
    parameters.editDist = editDist;
    return parameters;
  }

  static correctScorer(parameters){
    const foundSeq = parameters.foundSeq;
    const expSeq = parameters.expSeq;
    parameters.correct = 0;
    foundSeq.forEach(seq =>{
      if (seq === expSeq) {
        parameters.correct += 1;
      }
    });
    return parameters;
  }


  static createRawBIonGraph(parameters){
    const graphNodes = [];
    const bIonsSorted = parameters.bIons.sort((a, b) => {
      return a.mz - b.mz;
    });
    let prevBIonMz = bIonsSorted[0].mz;
    let prevIntensity = bIonsSorted[0].intensity;
    let prevSupport = bIonsSorted[0].support;
    for (let n = 1; n < bIonsSorted.length; n += 1) {
      const diff = Math.abs(prevBIonMz - bIonsSorted[n].mz);
      if (diff < parameters.createRawBIonTolerance) {
        const intensity = prevIntensity + bIonsSorted[n].intensity;
        prevBIonMz =
          (prevBIonMz * prevIntensity + bIonsSorted[n].mz * bIonsSorted[n].intensity) / intensity;
        prevIntensity = intensity;
        prevSupport += bIonsSorted[n].support;
      } else {
        // console.log(`${prevBIonMz} - ${prevIntensity}`);
        graphNodes.push(
          new Node(
            prevBIonMz,
            prevBIonMz,
            prevIntensity,
            prevSupport,
          )
        );
        prevBIonMz = bIonsSorted[n].mz;
        prevIntensity = bIonsSorted[n].intensity;
        prevSupport = bIonsSorted[n].support;
      }
    }
    graphNodes.push(new Node(prevBIonMz, prevBIonMz, prevIntensity, prevSupport));
    parameters.bIons = graphNodes;
    return parameters;
  }

  static getNodesFromEdges = (edge1, edge2) => {
    let nodes;
    if (edge1.source === edge2.source) {
      nodes = [edge1.target, edge2.target, edge1.source];
    }
    if (edge1.target === edge2.source) {
      nodes = [edge1.source, edge2.target, edge1.target];
    }
    if (edge1.source === edge2.target) {
      nodes = [edge1.target, edge2.source, edge2.target];
    }
    if (edge1.target === edge2.target) {
      nodes = [edge1.source, edge2.source, edge2.target]; // 1-3, 2-3, 1-2
    }
    return nodes; // not connected
  };

  static finishBIonGraph(parameters){
    const sortedBIons = parameters.bIons.sort((a, b) => {
      return a.mz - b.mz;
    });
    const graphEdges = [];
    let connection = false;
    const aminoAcidEdgeFactory = new AminoAcidEdgeFactory();
    const aminoAcidEdges = aminoAcidEdgeFactory._createAminoAcidEdgeArray();

    for (let o = 0; o < sortedBIons.length - 1; o += 1) {
      for (let i = o + 1; i < sortedBIons.length; i += 1) {
        const diff = sortedBIons[i].mz - sortedBIons[o].mz;
        for (let k = 0; k < aminoAcidEdges.length; k += 1) {
          if (
            diff >= aminoAcidEdges[k].monoisotopicMass - parameters.finishBIonTolerance &&
            diff <= aminoAcidEdges[k].monoisotopicMass + parameters.finishBIonTolerance
          ) {
            if (o === 0) connection = true;
            graphEdges.push(
              new Edge(
                Util.getKey(sortedBIons[i].mz, sortedBIons[o].mz),
                '',
                sortedBIons[o].mz,
                sortedBIons[i].mz,
                aminoAcidEdges[k].monoisotopicMass,
                diff,
                aminoAcidEdges[k].oneLetterCode
              )
            );
          }
        }
      }
    }
    if (!connection) {
      const aAPairs = aminoAcidEdgeFactory.aAPairs();
      for (let i = 1; i < sortedBIons.length; i += 1) {
        const diff = sortedBIons[i].mz - sortedBIons[0].mz;
        for (let k = 0; k < aAPairs.length; k += 1) {
          if (
            diff >= aAPairs[k].monoisotopicMass - parameters.finishBIonTolerance &&
            diff <= aAPairs[k].monoisotopicMass + parameters.finishBIonTolerance
          ) {
            graphEdges.push(
              new Edge(
                Util.getKey(sortedBIons[i].mz, sortedBIons[0].mz),
                '',
                sortedBIons[0].mz,
                sortedBIons[i].mz,
                aAPairs[k].monoisotopicMass,
                diff,
                aAPairs[k].oneLetterCode
              )
            );
          }
        }
      }
    }
    const graph = new Graph();
    graph.add(parameters.bIons, graphEdges);
    parameters.bIonGraph = graph;
    delete parameters.bIons;
    return parameters;
  }

  static createModelGraph(parameters){
    const graphNodes = new Map();
    // const modelMap = new Map();
    const msModelFactory = new MSModelFactory();
    const edges = Array.from(parameters.ionGraph.edges.values());
    for (let i = 0; i < edges.length; i += 1) {
      for (let j = i + 1; j < edges.length; j += 1) {
        if (edges[j].charge === edges[i].charge) {
          const cliqueNodeIds = this.getNodesFromEdges(edges[i], edges[j]);
          if (cliqueNodeIds !== undefined) {
            const edge3 = parameters.ionGraph.edges.get(Util.getKey(cliqueNodeIds[0], cliqueNodeIds[1])); // clique
            if (edge3 !== undefined) {
              if (edge3.charge === edges[i].charge) {
                const edgeId = Util.getKey3(edges[i].name, edges[j].name, edge3.name);
                const models = msModelFactory.createModelMidWeight(edgeId); // pattern
                if (models !== undefined) {
                  models.forEach(model => {
                    const id = Util.getKey4(
                      cliqueNodeIds[0],
                      cliqueNodeIds[1],
                      cliqueNodeIds[2],
                      model.id
                    );
                    if (!graphNodes.has(id)) {
                      const cliqueSortedNodeMzs = cliqueNodeIds
                        .sort((a, b) => {
                          return a - b;
                        })
                        .reverse();
                      const node1 = parameters.ionGraph.nodes.get(cliqueSortedNodeMzs[0]);
                      const node2 = parameters.ionGraph.nodes.get(cliqueSortedNodeMzs[1]);
                      const node3 = parameters.ionGraph.nodes.get(cliqueSortedNodeMzs[2]);

                      const names = model.getNodeNames();
                      const bIonMz1 = this.toBIon(parameters.pm,edge3.charge, names[0], cliqueSortedNodeMzs[0]); // 1-3
                      const bIonMz2 = this.toBIon(parameters.pm,edge3.charge, names[1], cliqueSortedNodeMzs[1]); // 2-3
                      const bIonMz3 = this.toBIon(parameters.pm,edge3.charge, names[2], cliqueSortedNodeMzs[2]); // 1-2
                      if (
                        Math.abs(bIonMz1 - bIonMz2) < parameters.createModelTolerance &&
                        Math.abs(bIonMz1 - bIonMz3) < parameters.createModelTolerance  &&
                        Math.abs(bIonMz2 - bIonMz3) < parameters.createModelTolerance
                      ) {
                        const intensity = node1.intensity + node2.intensity + node3.intensity;
                        const mz =
                          (bIonMz1 * node1.intensity +
                            bIonMz2 * node2.intensity +
                            bIonMz3 * node3.intensity) /
                          intensity;
                        /* if (!modelMap.has(model.id)) {
                        modelMap.set(model.id, new Array(`${mz}-${intensity}`));
                      } else {
                        const arr = modelMap.get(model.id);
                        arr.push(`${mz}-${intensity}`);
                      } */
                        /*                        console.log(
                          `mz: ${mz} charge: ${edge3.charge} id: ${model.id} mzs: (${
                            cliqueSortedNodeMzs[0]
                          }, ${cliqueSortedNodeMzs[1]}, ${
                            cliqueSortedNodeMzs[2]
                          }) b-ions: (${bIonMz1}, ${bIonMz2}, ${bIonMz3})(${names})`
                        ); */
                        // console.log(`${mz}-${intensity}-${model.id}-(${names})`);
                        if (graphNodes.has(mz)) {
                          const n = graphNodes.get(mz);
                          n.intensity += intensity;
                          n.support += 3;
                        } else {
                          graphNodes.set(mz, new Node(mz, mz, intensity, 3));
                        }
                      }
                    }
                  });
                }
              }
            }
          }
        }
      }
    }
    /*
    const start = nodes.find(node => Math.abs(1.0 - node.mz) < this.tolerance);
    if (start === undefined) {
      nodes.push(new Node(1.0, 1.0, 1.0, 3, 0, 0));
    }
    const pm = nodes.find(node => Math.abs(this.pm - node.mz) < this.tolerance);
    const pmNH3 = nodes.find(node => Math.abs(this.pm - 17.0265 - node.mz) < this.tolerance);
    if (pmNH3 !== undefined) {
      pmNH3.mz -= 0.9841;
    }
    if (pm !== undefined) {
      pm.mz -= 18.0098;
    } else {
      const medianIntensity = Util.medianIntensity(nodes);
      nodes.push(new Node(this.pm - 18.0098, this.pm - 18.0098, medianIntensity, 3, 0, 0));
    } */
    graphNodes.set(parameters.pm - 18.0098, new Node(parameters.pm - 18.0098, parameters.pm - 18.0098, 1, 3));
    graphNodes.set(1.0, new Node(1.0, 1.0, 1, 3));
    parameters.bIons = Array.from(graphNodes.values())
    delete parameters.ionGraph;
    return parameters;
  }

  static toBIon = (pm, charge, name, IonMz) => {
    const H = 1.0078;
    const mz = parseFloat(IonMz) * charge - charge*H + H;
    let bIon;
    switch(name) {
      case 'a':     bIon = mz + 27.9949;
                    break;
      case 'b':     bIon = mz;
                    break;
      case 'c':     bIon = mz - 17.0266;
                    break;
      case 'aH2O':  bIon = mz + 27.9949 + 18.0105;
                    break;
      case 'aNH3':  bIon = mz + 27.9949 + 17.0266;
                    break;
      case 'bNH3':  bIon = mz + 17.0266;
                    break;
      case 'bH2O':  bIon = mz + 18.0105;
                    break;
      case 'x':     bIon = pm - mz + 26.9865;
                    break;
      case 'y':     bIon = pm - mz + 1.0073;
                    break;
      case 'yH2O':  bIon = pm - mz - 17.0033;
                    break;
      case 'z':     bIon = pm - mz - 15.0115;
                    break;
      case 'yNH3':  bIon = pm - mz - 16.0193;
                    break;
      default:      console.log("an error occured: ",name," is unknown type");
    }
    return bIon;
  };
}
