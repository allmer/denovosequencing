export default class Node {
  constructor(id, mz, intensity, support) {
    this._id = id;
    this._mz = parseFloat(mz);
    this._intensity = parseFloat(intensity);
    this._support = support;
  }

  get id() {
    return this._id;
  }

  set id(value) {
    this._id = value;
  }

  get mz() {
    return this._mz;
  }

  set mz(value) {
    this._mz = value;
  }

  get intensity() {
    return this._intensity;
  }

  set intensity(value) {
    this._intensity = value;
  }

  get support() {
    return this._support;
  }

  set support(value) {
    this._support = value;
  }
}
