export default class Edge {
  constructor(id, classes, source, target, name, dist, label, charge) {
    this._id = id;
    this._classes = classes;
    this._source = source;
    this._target = target;
    this._name = name;
    this._dist = dist;
    this._label = label;
    this._charge = charge;
  }

  get classes() {
    return this._classes;
  }

  set classes(value) {
    this._classes = value;
  }

  get source() {
    return this._source;
  }

  set source(value) {
    this._source = value;
  }

  get target() {
    return this._target;
  }

  set target(value) {
    this._target = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get dist() {
    return this._dist;
  }

  set dist(value) {
    this._dist = value;
  }

  get label() {
    return this._label;
  }

  set label(value) {
    this._label = value;
  }

  get id() {
    return this._id;
  }

  set id(value) {
    this._id = value;
  }

  get charge() {
    return this._charge;
  }

  set charge(value) {
    this._charge = value;
  }
}
