
export default class Graph {
  constructor() {
    this.AdjList = new Map();
    this.edges = new Map();
    this.nodes = new Map();
  }

  addNode(v) {
    this.AdjList.set(v.id, []);
    this.nodes.set(v.id, v);
  }


  addEdge(e) {
    if (e.source < e.target) {
      this.AdjList.get(e.source).push(e.target);
    } else {
      this.AdjList.get(e.target).push(e.source);
    }
    this.edges.set(e.id, e);
  }

  connectedNodes(v) {
    return this.AdjList.get(v);
  }

  remove = () => {
    this.AdjList.clear();
    this.edges.clear();
    this.nodes.clear();
  };

  addNodes = nodes => {
    nodes.forEach(node => {
      this.addNode(node);
    });
  };
  removeNode(key) {
    this.AdjList.delete(key);
  }

  getNodeValues(key) {
    return this.AdjList.get(key);
  }
  addNode2(key, values) {
    this.AdjList.set(key, values);
  }
  addEdge2(source, target) {
    if (source < target) {
      this.AdjList.get(source).push(target);
    } else {
      this.AdjList.get(target).push(source);
    }
  }
  removeEdge(source, target) {
    if (source < target) {
      const list = this.AdjList.get(source);
      list.splice(list.indexOf(target), 1);
    } else {
      const list = this.AdjList.get(target);
      list.splice(list.indexOf(source), 1);
    }
  }


  addEdges = edges => {
    edges.forEach(edge => {
      this.addEdge(edge);
    });
  };

  add = (nodes, edges) => {
    nodes.forEach(node => {
      this.addNode(node);
    });
    edges.forEach(edge => {
      this.addEdge(edge);
    });
  };
}
