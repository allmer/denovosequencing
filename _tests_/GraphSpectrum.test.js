import GraphSpectrum from '../src/helper/GraphSpectrum.js';
import MSSpectrum from '../src/helper/MSSpectrum.js';
import {describe, expect} from "@jest/globals";

const mgfSection = [
  'BEGIN IONS',
  'PEPMASS=491.222686767578',
  'CHARGE=2',
  'TITLE=491.222686767578_1494.17_scan=6268_2014090922Mix2alkylISW10noEclu,seq={ATNYNAGDR},sup={4}',
  'SCANS=0',
  '491.2227\u00092',
  '128.1677\t34.3',
  '143.9659	14.8',
  '145.1864	1063.5',
  '147.2310	164.8',
  '148.0274	88.9',
  '152.2586	32.3',
  '153.1165	141.1',
  '155.0703	453.6',
  '156.2521	121.2',
  '158.0017	158.1',
  '162.1551	94.7',
  '163.1792	69.3'
];
const msms = new MSSpectrum();
msms.parseMGFSection(mgfSection);

describe('GraphSpectrum', () => {
  describe('get nodeList', () => {
    test('number of nodes correct', () => {
      const gms = new GraphSpectrum(msms.spectrum,0.3,msms.precursorMass);
      expect(gms.nodeList.length).toBe(12);
    });
  });
  describe('get nodes', () => {
    test('number of nodes correct', () => {
      const gms = new GraphSpectrum(msms.spectrum,0.3,msms.precursorMass);
      const nodes = gms.nodes;
      expect(nodes.length).toBe(12);
    });
    test('format correct', () => {
      const gms = new GraphSpectrum(msms.spectrum,0.3,msms.precursorMass);
      const nodes = gms.nodes;
      // expect(nodes[0].position.x).toBe(149);
      expect(nodes[0].id).toBe(128.1677);
    });
  });
  describe('get edges', () => {
    test('number of edges correct', () => {
      const gms = new GraphSpectrum(msms.spectrum,0.3,msms.precursorMass);
      const edges = gms.getEdges('sequence');
      expect(edges.length).toBe(11);
    });
  });
});
