import MSSpectrum from '../src/helper/MSSpectrum.js';
import {describe, expect} from "@jest/globals";

const mgfSection = [
  'BEGIN IONS',
  'PEPMASS=491.222686767578',
  'CHARGE=2',
  'TITLE=491.222686767578_1494.17_scan=6268_2014090922Mix2alkylISW10noEclu,seq={ATNYNAGDR},sup={4}',
  'SCANS=0',
  '491.2227\u00092',
  '128.1677\t34.3',
  '143.9659	14.8',
  '145.1864	1063.5',
  '147.2310	164.8',
  '148.0274	88.9',
  '152.2586	32.3',
  '153.1165	141.1',
  '155.0703	453.6',
  '156.2521	121.2',
  '158.0017	158.1',
  '162.1551	94.7',
  '163.1792	69.3'
];

const mgfSection2 = [
  'BEGIN IONS',
  'PEPMASS=491.222686767578',
  'CHARGE=2',
  'TITLE=491.222686767578_1494.17_scan=6268_2014090922Mix2alkylISW10noEclu,seq={ATNYNAGDR},sup={4}',
  'SCANS=0',
  '491.2227\u00092',
  '128.1677	34.3',
  '143.9659	14.8',
  '145.1864	1063.5',
  '147.2310	164.8',
  '148.0274	88.9',
  '152.2586	32.3',
  '153.1165	141.1',
  '155.0703	453.6',
  '156.2521	121.2',
  '158.0017	158.1',
  '162.1551	94.7',
  '482.1188	100',   //~PM + 2 - H20
  '483.2226	150',   //~PM + 2 - NH3
  '491	200',   //~PM + 2
  '145.1864	1063.5',
  '147.2310	164.8',
  '148.0274	88.9',
  '152.2586	32.3',
  '153.1165	141.1',
  '155.0703	453.6',
  '156.2521	121.2',
  '158.0017	158.1',
  '963.0033	100',  //~PM + 1 -H2O
  '964.1551	150',  //~PM + 1 -NH3
  '981.1792	400'    //~PM + 1
];

describe('MSSpectrum', () => {
  describe('parseMGFSection', () => {
    test('should succeed (tests name)', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      // console.log(msms);
      expect(msms.name).toBe(
        '491.222686767578_1494.17_scan=6268_2014090922Mix2alkylISW10noEclu,seq={ATNYNAGDR},sup={4}'
      );
    });
    test('should succeed (tests spec length)', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.spectrum.length).toBe(12);
    });
    test('should succeed (tests precursor)', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.precursorMass).toBeCloseTo(491.222686767578, 0.001);
    });
    test('should succeed (tests scan number)', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.scanNumber).toBe(0);
    });
    test('should succeed (tests sequence annotation)', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.sequence).toBe('ATNYNAGDR');
    });
    test('should succeed (tests support annotation)', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.support).toBe(4);
    });
    test('should succeed (test m/z and intensity', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.spectrum[0].mz).toBeCloseTo(128.1677);
      expect(msms.spectrum[1].intensity).toBeCloseTo(14.8);
      expect(msms.spectrum[10].mz).toBeCloseTo(162.1551);
      expect(msms.spectrum[11].intensity).toBeCloseTo(69.3);
    });
    test('should succeed (test getPeaks', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.getPeaks(145,5).length).toBe(4);
      expect(msms.getPeaks(145,5)[0].mz).toBeCloseTo(143.9659);
      expect(msms.getPeaks(145,5)[3].mz).toBeCloseTo(148.0274);
    });    
    test('should succeed (test getClosest', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.getClosest(145,5).mz).toBe(145.1864);
    });   
    test('should succeed (test getMostIntense', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection);
      expect(msms.getMostIntense(153,5).mz).toBe(155.0703);
    });      
    test('should fail (test adjustPrecursorMass', () => {
      const msms = new MSSpectrum();
      msms.parseMGFSection(mgfSection2);
      expect(msms.adjustPrecursorMass(1)).toBeCloseTo(981.2);
    });     
  });
});
